#include "hmi.h"
extern UG_GUI* gui;

HMI::HMI(PinName cs, PinName rs, PinName dc, PinName clk, PinName data):
SH1106(cs, rs, dc, clk, data) 
{
   // ��ʼ����Ļ
  initialise();
  set_font(standard_font, 16);
}

// ����
void HMI::UserDrawPixel(UG_S16 x, UG_S16 y, UG_COLOR c) {
  if (x >= SH1106_LCDWIDTH || y >= SH1106_LCDHEIGHT) return;
  if (c == C_BLACK)
     set_pixel((int)x, (int)y);
  else
     clear_pixel((int)x, (int)y);
            
}

