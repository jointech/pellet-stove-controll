#ifndef HMI_H
#define HMI_H

#include "ugui.h"
#include "sh1106.h"

class HMI: public SH1106 {
public:
  HMI(PinName cs, PinName rs, PinName dc, PinName clk, PinName data);
  
private:
  void UserDrawPixel(UG_S16 x, UG_S16 y, UG_COLOR c);
  UG_S16 UG_Init(UG_GUI* g, void (HMI::*p)(UG_S16,UG_S16,UG_COLOR), UG_S16 x, UG_S16 y);
  
};

#endif
