/* main.cpp
* 2015.11.18 15:03
*火炉控制板芯片使用 stm32f103vbt6
*v0.1
*/

#include "mbed.h"
#include "rtos.h"
#include "BusOut.h"
#include "BusInOut.h"

#include "keypad.h"
#include "ds1820.h"
#include "hmi.h"

Keypad keypad(KEY_ROW0,KEY_ROW1, KEY_ROW2, KEY_ROW3, KEY_COL0, KEY_COL1, KEY_COL2);  // 4*4矩阵键盘

//Serial serial(PA_2, PA_3);  // Serial (PinName tx, PinName rx)


DS1820 ds18b20(PE_2);  // ds18b20 传感器

// OLED: 

SH1106 hmi(OLED_CS, OLED_RST, OLED_DC, OLED_SCK, OLED_SDA);


//线程： 键盘扫描
void keypad_thread_callback(void const *argument) { 
  
  while(1){
    
    keypad.Scan_Keyboard();
    KeyPressed key_pressed = keypad.getKey();
    
    switch(key_pressed) {
        case Key_Up:
          //serial.printf("Up, 0x%2x\r\n", key_pressed);
          //oled.set_font(standard_font, 16);
          break;
          
        case Key_Down:
          //serial.printf("Down, 0x%2x\r\n", key_pressed);
          //oled.set_font(standard_font, 8);
          break;  
          
        case Key_On:
          //serial.printf("On, 0x%2x\r\n", key_pressed);
          break;
          
        default:
          if ( key_pressed != Key_None)
            //serial.printf("any key, 0x%2x\r\n", key_pressed);
            break;
    }
    
    if ( key_pressed != Key_None){
//        oled.clear();
//        oled.set_cursor(2,3);
//        oled.printf("风雨无阻");
//        oled.update();
    }
    
    Thread::wait(10);
  }
}

//线程： 读取时间
void readtime_thread_callback(void const *argument) {
  
 
}


int main()
{   
//  serial 
//  serial.baud(9600);
//  serial.format(); 
//  serial.printf("Hellow, world!\r\n");

  //Thread readtime_thread(readtime_thread_callback, NULL, osPriorityHigh); //启动rtc线程
  Thread keypad_thread(keypad_thread_callback); 
  DigitalOut led_0(PC_1);
  led_0 = 0;
  Thread::wait(1000);
  led_0 = 1;
  
  float temp;
  hmi.initialise();
  hmi.set_font(standard_font, 8);
  hmi.clear();
  hmi.printf("hellow");
  hmi.update();
  
    
  while (1) {
      // ds18b20
    if(!ds18b20.isPresent())
        ds18b20.begin();
    
    if (ds18b20.isPresent()) {
          ds18b20.startConversion(); 
          temp = ds18b20.read();  // 读取温度 1，放在主线程里
          //serial.printf("Temp. is %4.2f\r\n", temp);  
    }
    
        
      Thread::wait(2000);
  }
}
