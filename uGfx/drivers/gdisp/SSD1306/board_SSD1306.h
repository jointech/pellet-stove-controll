/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.org/license.html
 */

/**
 * @file    boards/addons/gdisp/board_SSD1306_spi.h
 * @brief   GDISP Graphic Driver subsystem board interface for the SSD1306 display.
 *
 * @note	This file contains a mix of hardware specific and operating system specific
 *			code. You will need to change it for your CPU and/or operating system.
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#include "mbed.h"
// The command byte to put on the front of each page line
//#define SSD1306_PAGE_PREFIX		0x40	// Co = 0, D/C = 1 不注释，则左侧会有7个像素显示

// For a multiple display configuration we would put all this in a structure and then
//	set g->board to that structure.

extern SPI oled_spi;
extern DigitalOut oled_spi_reset;
extern DigitalOut oled_spi_dc;
extern DigitalOut oled_spi_cs;
extern Serial serial;
   
extern SPI_HandleTypeDef holed_spi;

#define SET_RST oled_spi_reset = 0;  // 西兰花VET6板；
#define CLR_RST	oled_spi_reset = 1; 

static inline void init_board(GDisplay *g) {
	// As we are not using multiple displays we set g->board to NULL as we don't use it.
	g->board = 0;
}

static inline void post_init_board(GDisplay *g) {
	(void) g;
        oled_spi.format(8,3);
        oled_spi.frequency(1000000);
}

static inline void setpin_reset(GDisplay *g, bool_t state) {
	(void) g;
	if(state)
		SET_RST
	else
		CLR_RST
}

static inline void acquire_bus(GDisplay *g) {
	(void) g;
}

static inline void release_bus(GDisplay *g) {
	(void) g;
}

static inline void write_cmd(GDisplay *g, uint8_t cmd) {
	(void)	g;
        oled_spi_cs = 1;
        oled_spi_dc = 0;
        oled_spi_cs = 0;
        uint8_t r = oled_spi.write(cmd);
        //serial.printf("cmd:%0x,result:%0x\r\n", cmd, r);
        oled_spi_cs = 1;
}

static inline void write_data(GDisplay *g, uint8_t* data, uint16_t length) {
	(void) g;
        
        oled_spi_cs = 1;
        oled_spi_dc = 1;
        oled_spi_cs = 0;
        for (int i=0;i<length;i++)
        {
            oled_spi.write(*(data+i));
            //serial.printf("data[%d]:%0x\r\n", i, *(data+i));
        }
        oled_spi_cs = 1;
}


#endif /* _GDISP_LLD_BOARD_H */

