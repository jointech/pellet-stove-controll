/**************************************************************************************************
 *****                                                                                        *****
 *****  Name: KL25Z I2C_busreset.h                                                            *****
 *****  Date: 24/11/2013                                                                      *****
 *****  Auth: Frank Vannieuwkerke                                                             *****
 *****  Func: library for unblocking I2C bus on KL25Z board                                   *****
 *****  Info: MPL3115A2-AN4481                                                                *****
 **************************************************************************************************/

#include "mbed.h"

#ifndef KL25Z_I2C_RES_H
#define KL25Z_I2C_RES_H
 
void I2C_busreset(void);

#endif
