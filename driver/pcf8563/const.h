/***********************************
name:   const.h    Version: 0.1
author: PE HTL BULME
email:  pe@bulme.at
description:
  Named constants definitions for registers 
  PCF8563 RTC on HIMBED M0 - LPC11U24 
***********************************/
 
#ifndef CONST_H
#define CONST_H
 
// Address of RTC
const int RTC8563_ADR = 0xA2;
const int RTC8563_ADR_R = 0xA3;
// Control and status
const int CONTROL1 = 0x00;
const int CONTROL2 = 0x01;
// Time and date
const int SECONDS = 0x02;   
const int MINUTES = 0x03;
const int HOURS = 0x04;
const int DAYS = 0x05;
const int WEEKDAYS = 0x06;
const int MONTHS = 0x07;
const int YEARS = 0x08;
// Alarm
const int MINUTE_ALARM = 0x09;
const int HOUR_ALARM = 0x0A;
const int DAY_ALARM = 0x0B;
const int WEEKDAY_ALARM = 0x0C;
// Clock and timer
const int CLOCKOUT_FREQ = 0x0D;
const int TIMER_CINTROL = 0x0E;
 
#endif