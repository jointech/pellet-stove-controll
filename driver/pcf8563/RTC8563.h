//
//  @ Project : RTC8563
//  @ File Name : RTC8563.h
//  @ Date : 06.04.2015
//  @ Author : Franz Pucher
//  @ Copyright : pe@bulme.at
//
#if !defined(_RTC8563_H)
#define _RTC8563_H

#include "mbed.h"
//#include "I2C_busreset.h"

#define RTC8563_ADR 0xA2
#define RTC8563_ADR_R 0xA3

#define Addr_second 0x02 
#define Addr_minute 0x03 
#define Addr_hour 0x04 
#define Addr_day 0x05 
#define Addr_month 0x07 
#define Addr_year 0x08


typedef struct 
{ 
    unsigned char year; 
    unsigned char month; 
    unsigned char day; 
    unsigned char hour; 
    unsigned char minute; 
    unsigned char second;
    unsigned char week;
} TIME;

class RTC8563
{
public:
    RTC8563(PinName sda, PinName scl);
    TIME* rtc_read(void);
    
    TIME _time; 

private:
  
  void rtc_write(char address, char value); 
  
  char _failure_count;  //д��ַʧ�ܼ���
  I2C _i2c;

    
};
#endif  //_RTC8563_H