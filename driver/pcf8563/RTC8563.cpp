//
//  @ Project : RTC Date Time Clock
//  @ File Name : RTC8563.cpp
//  @ Date : 06.04.2015
//  @ Author : Franz Pucher
//  @ Copyright : pe@bulme.at
//
 
#include "mbed.h"
#include "RTC8563.h"
 
RTC8563::RTC8563(PinName sda, PinName scl) : _i2c(sda, scl)
{
   // Initialise I2C

    _failure_count = 0;
     
}

 
void RTC8563::rtc_write(char address, char value)
{
    _i2c.start();
    _i2c.write(RTC8563_ADR);
    _i2c.write(address);
    _i2c.write(value);
    _i2c.stop();
}
 
TIME* RTC8563::rtc_read(void)
{
    char temp;
    char data[7] = {0};  // 7个数据
    char address = 0x02;
    
    if( _i2c.write(RTC8563_ADR, &address, 1) != -1){
      
      if(_i2c.read(RTC8563_ADR, data, 6) == 0){  // 6次
        temp = *data & 0x7f; // 1 秒
        temp = (temp / 16) * 10 + temp % 16; 
        _time.second = temp ;

        temp = *(data + 1) & 0x7f; // 2 分
        temp = (temp / 16) * 10 + temp % 16; 
        _time.minute = temp;

        temp = *(data + 2) & 0x3f; // 3 时
        temp = (temp / 16) * 10 + temp % 16; 
        _time.hour = temp; 

        temp = *(data + 3) & 0x3f; // 4 日
        temp = (temp / 16) * 10 + temp % 16; 
        _time.day = temp; 
        
        temp = *(data + 4) & 0x07; // 5 星期
        temp = (temp / 16) * 10 + temp % 16; 
        _time.week = temp;

        temp = *(data + 5) & 0x1f; // 6 月
        temp = (temp / 16) * 10 + temp % 16; 
        _time.month = temp;

        _time.year = *(data + 6); // 7 年

      }
      else
        _failure_count++;   
      
    }
        
    else
      _failure_count++;
    
    //如果5次失败，启动i2c总线复位
    if(_failure_count>=5){
      //I2C_busreset();  
      _failure_count = 0;
    }
    
        return &_time;
  
}
 
