/*
*
* Name: keypad.cpp
*
*/

#include "keypad.h"

//构造函数
Keypad::Keypad(PinName row0, PinName row1, PinName row2, PinName row3,
               PinName col0, PinName col1, PinName col2, PinName col3):
  _row0(row0), _row1(row1), _row2(row2), _row3(row3),
  _cols(col0, col1, col2, col3)
{
     
  _rows[0] = &_row0;
  _rows[1] = &_row1;
  _rows[2] = &_row2;
  _rows[3] = &_row3;
	 
   for (int r = 0; r < row_count; r++)
      _rows[r]->mode(PullUp); 
  
   //_cols设置为输出
   _cols.output();
   //_cols = 0x0f;
   
   KeyState = 0; // 按键状态初始 0
}

//扫描键盘
void Keypad::Scan_Keyboard(void){
switch (KeyState) {
  case 0: {
  if (AnyKey()) {    
    char scankey = ScanKey();
    if (scankey != 0xff)
        KeyCode = scankey;
    
    KeyState = 1;
  }
  
  break;
  }
  case 1: {
  if (SameKey()) {
    FindKey();
    KeyState = 2;  
  }
  else 
    KeyState = 0;
  
  break;
  }
  case 2: {
    if (SameKey()) {
    }
    else 
      KeyState = 3;
    
    break;
    }
  case 3: {
    if (SameKey()) {
      KeyState = 2;
    }
    else {
      ClearKey();
      KeyState = 0;
    }
    
    break;
   }
  }
// func end
}

// 有键按下
char Keypad::AnyKey(void) {
 //Start();  //拉低
 
 int r = -1;
 for (r = 0; r < row_count; r++) {
     if (*_rows[r] == 0)
         break;	 
 }
 
 //Stop();  //恢复
 
 if (!(0 <= r && r < row_count))
    return 0;
 else
    return 1;
}

// 键按下， 键值相同
char Keypad::SameKey(void) {
  
  // char KeyCode_new = KeyCode;
  char KeyCode_new = ScanKey();
  if (KeyCode == KeyCode_new)  
    return 1;
  else
    return 0;
}

// 扫描键
char Keypad::ScanKey(void) {

 /* 行扫描 */
    int r = -1;
    for (r = 0; r < row_count; r++) {
        if (*_rows[r] == 0)
            break;
    }

    /* 若没有找到有效行，返回 */
    if (!(0 <= r && r < row_count)) {
      return 0xff;
        
    }

    /* 列扫描，找出行上哪个被拉低 */
    int c = -1;
    for (c = 0; c < col_count; c++) {
        _cols = ~(1 << c);
        if (*_rows[r] == 0)
            break;
    }

    /* 给所有的列重新充电 */
    Start();

    /* 若没有找到有效列，返回 */
    if (!(0 <= c && c < col_count))
        return 0xff;

    //KeyCode = r * col_count + c;
    //return 1;
    return r * col_count + c;
 
}

// FindKey compares KeyCode to values in KeyTable.
// If match, KeyValue, KeyDown and KeyNew are updated.
void Keypad::FindKey(void) {
 KeyValue = (KeyPressed)KeyCode;
 KeyDown = 1;
 KeyNew = 1;
}


void Keypad::ClearKey(void) {
 KeyDown = 0;
}

KeyPressed Keypad::getKey(void) {
  if(KeyNew)
  {
    KeyNew = 0;
    return KeyValue;
  }
  else
    return Key_None;
}

void Keypad::Start(void)
{
    /* 列输出0， 拉低行 */
    _cols = 0x00;
}

void Keypad::Stop(void)
{
    /* 列输出1，使行不被拉低 */
    _cols = 0x0f;
}
