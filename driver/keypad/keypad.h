/*
*
* Name: keypad.h
*/
#ifndef KEYPAD_H
#define KEYPAD_H

#include "mbed.h"
#include "BusInOut.h"

/* 
*  0  1  2   3
*  4  5  6   7
*  8  9  10  11
*  12 13 14  15
*/

typedef enum {
    Key_Up = 0x02,
    Key_Left = 0x05,
    Key_Right = 0x07,
    Key_Down = 0x0a,
    Key_On =  0x03,
    Key_Mode = 0x11,
    Key_None = 0xFF
} KeyPressed;


class Keypad{
public:
  Keypad(PinName row0, PinName row1, PinName row2, PinName row3,
         PinName col0, PinName col1, PinName col2, PinName col3=NC);
  void Scan_Keyboard(void);
  KeyPressed getKey(void);

  
private:  
  char AnyKey(void);
  char SameKey(void);
  char ScanKey(void);
  void FindKey(void);
  void ClearKey(void);
  void Read(void);
	/** Start the keypad interrupt routines */
  void Start(void);
  /** Stop the keypad interrupt routines */
  void Stop(void);
  
  // State:
  char KeyState;
  // Bit pattern after each scan:
  char KeyCode;
  // Output value from the virtual 74HC922:
  KeyPressed KeyValue;
  // KeyDown is set if key is down:
  char KeyDown;
  // KeyNew is set every time a new key is down:
  char KeyNew;
  // ӳ���
  char KeyTable[12][2];
	
  static const int row_count = 4;
  static const int col_count = 3;

  BusInOut       _cols;
  DigitalIn      _row0;
  DigitalIn      _row1;
  DigitalIn      _row2;
  DigitalIn      _row3;
  DigitalIn      *_rows[row_count];
  
  
};

#endif // KEYPAD_H